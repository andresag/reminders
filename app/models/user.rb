class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  has_many :reminders

  validates_presence_of :email
  validates_format_of :email, with: URI::MailTo::EMAIL_REGEXP
end
