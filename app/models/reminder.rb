class Reminder < ApplicationRecord
  belongs_to :user

  validates_presence_of :title, :description, :month_day

  scope :not_dispatched, -> do
    where('month_day <= ? AND time <= ? AND (last_reminder_sent < ? OR last_reminder_sent IS NULL)', Date.today.day, Time.now, DateTime.now)
  end

  def dispatched?
    month_day <= Date.today.day && last_reminder_sent.present? && last_reminder_sent.month == Date.today.month
  end
end
