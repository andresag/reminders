class ApplicationPolicy
  attr_accessor :user, :model

  def initialize(user, model)
    @user  = user
    @model = model
  end

  def can?(action, &block)
    return true if eval("#{action}?")
    raise StandardError, 'not authorized'
  rescue StandardError
    block&.call($!.message)
    false
  end

  def index?
    raise NotImplementedError
  end

  def show?
    raise NotImplementedError
  end

  def create?
    raise NotImplementedError
  end

  def update?
    raise NotImplementedError
  end

  def destroy?
    raise NotImplementedError
  end
end
