class ReminderPolicy < ApplicationPolicy
  def show?
    in_user_scope?
  end

  def edit?
    in_user_scope?
  end

  def update?
    in_user_scope?
  end

  def destroy?
    in_user_scope?
  end

  private

  def in_user_scope?
    user.reminders.include? model
  end
end
