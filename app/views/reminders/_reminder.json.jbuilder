json.extract! reminder, :id, :title, :description, :month_day, :time, :created_at, :updated_at
json.url reminder_url(reminder, format: :json)
