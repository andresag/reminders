class ReminderDispatcherService
  def self.perform
    Reminder.not_dispatched.each do |reminder|
      UserMailer.reminder(reminder, reminder.user).deliver_now
      reminder.update(last_reminder_sent: DateTime.now)
    end
  end
end
