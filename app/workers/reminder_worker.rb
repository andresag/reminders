require 'sidekiq-scheduler'

class ReminderWorker
  include Sidekiq::Worker

  def perform
    ReminderDispatcherService.perform
  end
end
