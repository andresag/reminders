class UserMailer < ApplicationMailer
  def reminder(reminder, user)
    @reminder  = reminder
    @user = user
    mail(to: @user.email, subject: @reminder.title)
  end
end
