require 'rails_helper'
require 'pry'

RSpec.describe RemindersController, type: :controller do
  let(:user) { create(:user) }
  let!(:reminder) { create(:reminder, user: user) }

  before { sign_in user }

  describe 'GET /reminders' do
    before { get :index }

    it { expect(response).to have_http_status(:ok) }
    it { expect(response).to render_template(:index) }
    it { expect(assigns(:reminders)).to eq([reminder]) }
  end

  describe 'GET /reminders/:id' do
    context 'when the record exists' do
      before { get :show, params: { id: reminder.id } }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:show) }
      it { expect(assigns(:reminder)).to eq(reminder) }
    end

    context 'when the record belongs to other customer' do
      let(:user2) { create(:user) }
      let(:reminder2) { create(:reminder, user: user2) }

      before { get :show, params: { id: reminder2.id } }

      it { expect(response).to have_http_status(:forbidden) }
    end
  end


  describe 'GET /reminders/new' do
    before { get :new }

    it { expect(response).to have_http_status(:ok) }
    it { expect(response).to render_template(:new) }
  end

  describe 'GET /reminders/:id/edit' do
    context 'when the record exists' do
      before { get :edit, params: { id: reminder.id } }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:edit) }
    end

    context 'when the record belongs to other customer' do
      let(:user2) { create(:user) }
      let(:reminder2) { create(:reminder, user: user2) }

      before { get :edit, params: { id: reminder2.id } }

      it { expect(response).to have_http_status(:forbidden) }
    end
  end

  describe 'POST /reminders' do
    context 'when the request is valid' do
      before { post :create, params: { reminder: { title: Faker::Lorem.word, description: Faker::Lorem.sentence(1), month_day: 1 } } }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(assigns(:reminder)).to be_persisted }
      it { expect(response).to redirect_to(assigns(:reminder)) }
    end

    context 'when the request is invalid' do
      before { post :create, params: { reminder: { title: '' } } }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:new) }
      it { expect(assigns(:reminder)).to_not be_persisted }
    end
  end

  describe 'PUT /reminders/:id' do
    context 'when the request is valid' do
      before { put :update, params: { id: reminder.id, reminder: { title: Faker::Lorem.word, description: Faker::Lorem.sentence(1), month_day: 1 } } }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(reminder) }
      it { expect(assigns(:reminder)).to be_persisted }
    end

    context 'when the request is invalid' do
      before { put :update, params: { id: reminder.id, reminder: { title: '' } } }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:edit) }
      it { expect(assigns(:reminder)).to_not be_valid }
    end

    context 'when the record belongs to other customer' do
      let(:user2) { create(:user) }
      let(:reminder2) { create(:reminder, user: user2) }

      before { put :update, params: { id: reminder2.id, reminder: { title: '' } } }

      it { expect(response).to have_http_status(:forbidden) }
    end
  end

  describe 'DELETE /reminders/:id' do
    context 'when the record can be destroyed' do
      before { delete :destroy, params: { id: reminder.id } }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(reminders_url) }
      it { expect(assigns(:reminder)).to_not be_persisted }
    end

    context 'when the record belongs to other customer' do
      let(:user2) { create(:user) }
      let(:reminder2) { create(:reminder, user: user2) }

      before { delete :destroy, params: { id: reminder2.id } }

      it { expect(response).to have_http_status(:forbidden) }
    end
  end
end
