require 'rails_helper'
require 'pry'

RSpec.describe ReminderDispatcherService do
  let!(:past_reminder) { create(:reminder, month_day: Date.yesterday.day) }
  let!(:future_reminder) { create(:reminder, month_day: Date.tomorrow.day) }

  context '#perform' do
    before { ReminderDispatcherService.perform }

    it { expect(past_reminder.reload.dispatched?).to be_truthy }
    it { expect(future_reminder.reload.dispatched?).to be_falsey }
  end
end
