require 'rails_helper'

RSpec.describe Reminder, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:month_day) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:user) }
  end
end
