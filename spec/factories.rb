require 'faker'

FactoryBot.define do
  factory :reminder do
    title { Faker::Lorem.word }
    description { Faker::Lorem.sentence(1) }
    month_day { 1 }
    time { Time.now }
    user
  end

  factory :user do
    email { Faker::Internet.email }
    password { SecureRandom.uuid[0..6] }
  end
end