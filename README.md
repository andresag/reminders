# Quick explanation.

The app uses Devise for authentication, although the only focus for this
was to be able to set current_user and a small Policy class for authorization.

## /controllers
The important one is the reminders controller. This is basically a scaffold, but added a before_action :authorize! for some
of the actions plus the index and queries are scoped under the current_user

## /mailers
This only contains the user mailers with the reminder email. Nothing special here

## /models
You will find some validations in the models

## /policies
You will find the AppPolicy and the ReminderPolicy, this is to be able to determine the authorization in the controllers,
so if something is out of the scope of the user, it will render unauthorized

## /services
Small service to dispatch the reminders that have to be sent

## /workers
The reminder worker which is called by sidekiq scheduler everyminute, which is just calling the ReminderDispatcherService

## /spec
Couple tests were done. For the reminders_controller, all actions have been tested accordingly.
The models contain validations and associations tests.
The services just checks if the correct reminders are dispatched.

=======================================

Sample Task: Scheduled Reminders

Description: write an application that sends scheduled reminders to its users.

Functionality:

- a user can register with email and password (for example via Devise)
- after signing in, they see a list of existing reminders and can set up new ones
- once a month, on a configurable day and time, the application sends them an email with the reminder title and text
- existing reminders can be deleted from the list

Configuration of the reminder:
- title
- description / text
- day and time of month. Should be any possible day of the month. I.e. "1st of month", "2nd of month", but also "last of month", "2nd last month"
